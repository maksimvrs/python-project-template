from tartiflette_asgi import TartifletteApp


app = TartifletteApp(
    engine=None,
    sdl='',
    graphiql=True,
    path="/",
    subscriptions=None,
    context={},
    schema_name="default",
)
