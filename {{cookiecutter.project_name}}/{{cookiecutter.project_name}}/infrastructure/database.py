from typing import Optional

from dependency_injector import resources
from sqlalchemy.engine import Engine
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker


class AsyncDatabaseConnection(resources.AsyncResource):
    def __init__(self) -> None:
        super().__init__()
        self.engine: Optional[Engine] = None
        self.session: Optional[AsyncSession] = None

    async def init(self, settings):
        self.engine = create_async_engine(
            "postgresql+asyncpg://scott:tiger@localhost/test",
            echo=True,
        )
        self.async_session = sessionmaker(
            self.engine, expire_on_commit=False, class_=AsyncSession
        )
        yield await connect()

    async def shutdown(self, connection):
        await connection.close()
