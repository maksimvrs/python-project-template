"""Main container."""

from dependency_injector import containers, providers
from dynaconf import settings

from infrastructure.database import AsyncDatabaseConnection


class Container(containers.DeclarativeContainer):

    settings = providers.Singleton(settings)
    database = providers.Resource(
        AsyncDatabaseConnection,
        settings=settings,
    )
