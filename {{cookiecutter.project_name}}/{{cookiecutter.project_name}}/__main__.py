#!/usr/bin/env python
import sys

from appliction.entrypoint import run
from containers.main import Container


if __name__ == "__main__":
    container = Container()
    container.wire(modules=[sys.modules[__name__]])
    sys.exit(run())
