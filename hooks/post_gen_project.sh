#!/bin/bash

curl https://bootstrap.pypa.io/get-pip.py | $(which python3)

$(which python3) -m pip install --upgrade pip

$(which python3) -m pip install venv

$(which python3) -m venv env

source env/bin/activate

env/bin/python3 -m pip install --upgrade pip

env/bin/pip install alembic

# Install poetry is not exists

if ! poetry -V &> /dev/null
then
    curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | $(which python3) -
fi

poetry init

poetry add dynaconf dependency-injector

alembic init migrations

deactivate
